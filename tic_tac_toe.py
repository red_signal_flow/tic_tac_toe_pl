from pygame.font import Font
from pygame import draw,Rect,mouse
from pyswip import Prolog
from player import Player

BLACK=  (0,0,0)
BLUE=   (0,0,255)
RED=    (255,0,0)
GREEN=  (0,255,0)

BOARD_SIZE = (600,600)
POSITION_MAP =[None,
               (0,0,200,200),(200,0,200,200),(400,0,200,200),
               (0,200,200,200),(200,200,200,200),(400,200,200,200),
               (0,400,200,200),(200,400,200,200),(400,400,200,200)]

class Square(object):
    def __init__(self,surface,area,rect):
        self.surface = surface
        self.rect = rect
        self.area = area
        self.value = None
        self.letter = None

    def set_value(self,value):
        self.value = value

    def hover(self):
        m_x,m_y,m_w,m_h = self.area
        x,y = mouse.get_pos()
        if  x > m_x and x < m_x+m_w and y > m_y and y < m_y+m_h :
            return True
        return False

    def draw(self):
        if self.value:
            self.surface.blit(self.value,self.rect)

class Board(object):
    def __init__(self,surface,player):
        self.surface = surface
        font = Font(None,350)
        self.o = font.render("O",1,RED)
        self.x = font.render("X",1,GREEN)
        self.position = (0,0)
        self.squares = [None]
        self.generate_squares()
        self.prolog = Prolog()
        self.load_IA()
        self.current_player = 0
        self.player = player
        self.player.set_value(self.x)
        self.player.set_piece("xis")
        self.player.letter = 'X'

    def generate_squares(self):
        for i in range(9):
            rect = Rect(POSITION_MAP[i+1])
            area = POSITION_MAP[i+1]
            s = Square(self.surface,area,rect)
            self.squares.append(s)


    def draw(self):
        sw,sh = BOARD_SIZE
        # draw vertical
        draw.line(self.surface,BLACK,[sw/3,0],[sw/3,sh-0],5)
        draw.line(self.surface,BLACK,[2*sw/3,0],[2*sw/3,sh-0],5)
        #draw horizontal
        draw.line(self.surface,BLACK,[0,sh/3],[sw-0,sh/3],5)
        draw.line(self.surface,BLACK,[0,2*sh/3],[sw-0,2*sh/3],5)
        for s in self.squares:
            if s:
                s.draw()

    def load_IA(self):
       self.prolog.consult('prolog/can_win.pl')
       self.prolog.consult('prolog/winner.pl')
       self.prolog.consult('prolog/play.pl')
       self.prolog.consult('prolog/rules.pl')
       self.prolog.consult('prolog/util.pl')

    def play_bot(self):
        if self.is_game_finished():
            return

        if self.current_player == 1:
            for l in self.prolog.query("play(Area,circulo)"):
                self.squares[l['Area']].set_value(self.o)
                self.squares[l['Area']].letter = 'O'
                try_play = False


        self.current_player = 0

    def update(self):
        if self.is_game_finished():
            return

        for i in range(len(self.squares)):
            square = self.squares[i]
            if square  and square.hover() and self.current_player == 0:
               square.set_value(self.player.value)
               square.letter = self.player.letter
               query = "use_area({0},{1})".format((i),self.player.piece)
               self.prolog.query(query).next()
               self.current_player = 1

    def is_game_finished(self):
        winner = self.get_winner()
        all_square_with_value = True

        for square in self.squares:
            if square and square.value == None:
                all_square_with_value = False

        return (winner != None) or (all_square_with_value)

    def get_winner(self):
        winner = None
        squares = self.squares

        for i in [1, 4, 7]:
            if (squares[i].letter == squares[i+1].letter == squares[i+2].letter != None):
                winner = squares[i].letter

        for i in [1, 2, 3]:
            if (squares[i].letter == squares[i+3].letter == squares[i+6].letter != None):
                winner = squares[i].letter

        if (squares[1].letter == squares[5].letter == squares[9].letter != None):
                winner = squares[1].letter

        if (squares[3].letter == squares[5].letter == squares[7].letter != None):
                winner = squares[3].letter

        if winner == 'X':
            winner = self.player.name
        if winner == 'O':
            winner = 'I.A.'

        return winner

    def is_used_square(self, index):
        return self.squares[index].letter != None