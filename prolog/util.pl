% corner
corner(1).
corner(3).
corner(7).
corner(9).
middle(5).

% between areas
between(2,1,3).
between(4,1,7).
between(5,1,9).
between(5,3,7).
between(5,2,8).
between(5,4,6).
between(8,7,9).
between(6,3,9).

% used areas get pieces
between(Area,Piece):- between(Area,Area1,Area2),used_area(Area1,Piece),used_area(Area2,Piece).
corner(Area,Piece):- corner(Area),used_area(Area,Piece).

%plays
can_play(Area):- empty(Area).
search_corner_unused(Area):- corner(Area),can_play(Area).
search_between_unused(Area):- can_play(Area), between(Area,_).

