% play to the other play not to win
play(Area,Piece):- can_win(Area,circulo),use_area(Area,Piece),!.
play(Area,Piece):- can_win(Area,xis),use_area(Area,Piece),!.

% if first play
play(Area,Piece):- search_corner_unused(Area),use_area(Area,Piece),!.

% between pieces
play(Area,Piece):- search_between_unused(Area),use_area(Area,Piece),!.


% if second play
play(Area,Piece):- search_corner_unused(Area),use_area(Area,Piece),!.

% if not in middle use a middle area
play(Area,Piece):- not(used_area(5,Piece)),use_area(Area,Piece),!.

