% dynamic
% used(Area/Piece)
:- dynamic used/2.
% empty (Area)
:- dynamic empty/1.
% my_piece
:- dynamic my_piece/1.

%   truthy
piece(circulo).
piece(xis).

area(Area) :- Area > 0,Area < 10.

% begin all 
empty(1).
empty(2).
empty(3).
empty(4).
empty(5).
empty(6).
empty(7).
empty(8).
empty(9).

% verify unused area
empty_area(Area):- empty(Area).

% verify_used area
used_area(Area,X) :- not(empty(Area)),used(Area,X).
%use a empty area
use_area(Area,Piece):- assertz(used(Area,Piece)),retract(empty(Area)).
