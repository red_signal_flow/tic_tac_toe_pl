% horizontal  1,2,3
can_win(Area,Piece):- used_area(2,Piece),used_area(3,Piece),empty_area(1), Area = 1,!.
can_win(Area,Piece):- used_area(1,Piece),used_area(3,Piece),empty_area(2), Area = 2,!.
can_win(Area,Piece):- used_area(1,Piece),used_area(2,Piece),empty_area(3), Area = 3,!.
% horizontal 4,5,6
can_win(Area,Piece):- used_area(5,Piece),used_area(6,Piece),empty_area(4), Area = 4,!.
can_win(Area,Piece):- used_area(4,Piece),used_area(6,Piece),empty_area(5), Area = 5,!.
can_win(Area,Piece):- used_area(4,Piece),used_area(5,Piece),empty_area(6), Area = 6,!.
% horizontal 7,8,9
can_win(Area,Piece):- used_area(8,Piece),used_area(9,Piece),empty_area(7), Area = 7,!.
can_win(Area,Piece):- used_area(7,Piece),used_area(9,Piece),empty_area(8), Area = 8,!.
can_win(Area,Piece):- used_area(7,Piece),used_area(8,Piece),empty_area(9), Area = 9,!.

% vertical 1,4,7
can_win(Area,Piece):- used_area(4,Piece),used_area(7,Piece),empty_area(1), Area = 1,!.
can_win(Area,Piece):- used_area(1,Piece),used_area(7,Piece),empty_area(4), Area = 4,!.
can_win(Area,Piece):- used_area(1,Piece),used_area(4,Piece),empty_area(7), Area = 7,!.
% vertical 2,5,8
can_win(Area,Piece):- used_area(5,Piece),used_area(8,Piece),empty_area(2), Area = 2,!.
can_win(Area,Piece):- used_area(2,Piece),used_area(8,Piece),empty_area(5), Area = 5,!.
can_win(Area,Piece):- used_area(2,Piece),used_area(5,Piece),empty_area(8), Area = 8,!.
% vertical 3,6,9
can_win(Area,Piece):- used_area(6,Piece),used_area(9,Piece),empty_area(3), Area = 3,!.
can_win(Area,Piece):- used_area(3,Piece),used_area(9,Piece),empty_area(6), Area = 6,!.
can_win(Area,Piece):- used_area(3,Piece),used_area(6,Piece),empty_area(9), Area = 9,!.

% diagonal 1,5,9
can_win(Area,Piece):- used_area(5,Piece),used_area(9,Piece),empty_area(1), Area = 1,!.
can_win(Area,Piece):- used_area(1,Piece),used_area(9,Piece),empty_area(5), Area = 5,!.
can_win(Area,Piece):- used_area(1,Piece),used_area(5,Piece),empty_area(9), Area = 9,!.

% diagonal 3,5,7
can_win(Area,Piece):- used_area(5,Piece),used_area(7,Piece),empty_area(3), Area = 3,!.
can_win(Area,Piece):- used_area(3,Piece),used_area(7,Piece),empty_area(5), Area = 5,!.
can_win(Area,Piece):- used_area(3,Piece),used_area(5,Piece),empty_area(7), Area = 7,!.
