% horizontal  1,2,3
winner(Piece):- used_area(2,Piece),used_area(3,Piece),used_area(1,Piece),!.
% horizontal 4,5,6
winner(Piece):- used_area(5,Piece),used_area(6,Piece),used_area(4,Piece),!.
% horizontal 7,8,9
winner(Piece):- used_area(8,Piece),used_area(9,Piece),used_area(7,Piece),!.
% vertical 1,4,7
winner(Piece):- used_area(4,Piece),used_area(7,Piece),used_area(1,Piece),!.
% vertical 2,5,8
winner(Piece):- used_area(5,Piece),used_area(8,Piece),used_area(2,Piece),!.
% vertical 3,6,9
winner(Piece):- used_area(6,Piece),used_area(9,Piece),used_area(3,Piece),!.
% diagonal 1,5,9
winner(Piece):- used_area(5,Piece),used_area(9,Piece),used_area(1,Piece),!.
% diagonal 3,5,7
winner(Piece):- used_area(5,Piece),used_area(7,Piece),used_area(3,Piece),!.
