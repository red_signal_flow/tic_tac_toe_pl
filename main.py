from pygame import event, mouse, init, display
from pygame.time import Clock
from pygame.locals import *
from pygame.font import Font

from player import Player
from tic_tac_toe import Board
import sys

def update(surface):
    surface.fill([255,255,255])

BLACK=  (0,0,0)

FRAME_RATE = 60
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_DIMENSION = (SCREEN_WIDTH, SCREEN_HEIGHT)

init()
clock = Clock()
running = True
surface = display.set_mode(SCREEN_DIMENSION, DOUBLEBUF)
player = Player(sys.argv[1])
b = Board(surface,player)
rect = Rect((0,200,200,200))

while running:
    update(surface)
    clock.tick(FRAME_RATE)
    b.draw()
    for e in event.get():
        if e.type==QUIT:
            running = False
        if e.type==MOUSEBUTTONDOWN:
            b.update()

    b.play_bot()

    if(b.is_game_finished()):
        winer = b.get_winner()

        font = Font(None,75)
        if(winer != None):
            text_render = font.render("Vencedor: [%s]" % winer,1,BLACK)
            surface.blit(text_render, rect)
        else:
            text_render = font.render("Deu Velha",1,BLACK)
            surface.blit(text_render, rect)

    display.flip()



